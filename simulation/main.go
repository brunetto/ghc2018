package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/brunetto/goutils/readfile"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Simulation struct {
	rows               int
	cols               int
	vehicles           Vehicles
	rides              Rides
	fleetSize          int
	ridesNumber        int
	bonus              int
	SimulationDuration int
	outFileName        string
}

func NewSimulation(inFileName, outFileName string) (*Simulation, error) {
	if outFileName == "" {
		outFileName = strings.TrimSuffix(inFileName, filepath.Ext(inFileName)) + ".out"
	}

	s := &Simulation{}
	s.outFileName = outFileName

	file, err := os.Open(inFileName)
	if err != nil {
		return s, errors.Wrap(err, "error opening input file")
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	lr := readfile.NewLineReader(reader)

	tmp, err, _ := lr.LineToIntSlice(" ")
	if err != nil {
		return s, errors.Wrap(err, "error reading first line of input file")
	}

	if len(tmp) < 6 {
		return s, errors.New("not enough values in the first file line of input file")
	}
	s.rows = tmp[0]
	s.cols = tmp[1]
	s.fleetSize = tmp[2]
	s.ridesNumber = tmp[3]
	s.bonus = tmp[4]
	s.SimulationDuration = tmp[5]

	for i := 0; i < s.fleetSize; i++ {
		s.vehicles = append(s.vehicles, NewVehicle(i))
	}

	rides := Rides{}
	for i := 0; i < s.ridesNumber; i++ {
		tmp, err, eof := lr.LineToIntSlice(" ")
		if err != nil {
			if eof {
				err = errors.Wrap(err, "unexpected EOF")
			}
			err = errors.Wrap(err, "error loading data")
			return s, err
		}

		ax := tmp[0]
		ay := tmp[1]
		bx := tmp[2]
		by := tmp[3]
		t1 := tmp[4]
		t2 := tmp[5]

		rides = append(rides, NewRide(i, ax, ay, t1, bx, by, t2))

	}
	s.rides = rides

	return s, nil
}

func (s *Simulation) Run() error {
	//log := s.log
	rides := map[int]Ride{}
	for _, r := range s.rides {
		rides[r.ID] = r
	}

sim:
	for step := 0; step < s.SimulationDuration; step++ {

		// for each step, for each vehicle, find the best ride
		// TODO: it depends on the vehicle order, I'll improve this later
		for _, v := range s.vehicles {

			// if vehicle is occupied, skip
			if step < v.OccupiedUntil {
				continue
			}

			// find the best ride
			referenceWeight := -1
			referenceID := -1
			referenceEnding := -1
			for i, r := range rides {

				// too late my friend
				if step+r.Duration > r.Stop.Time {
					delete(rides, r.ID)
					continue
				}

				distanceFromStart := SpaceDistance(v.Position, r.Start.Position)
				// if we arrive too early we need to wait
				if step+distanceFromStart < r.Start.Time {
					distanceFromStart = distanceFromStart + int(math.Abs(float64(r.Start.Time-step+distanceFromStart)))
				}

				runStop := step + distanceFromStart + r.Duration
				if runStop >= s.SimulationDuration {
					continue // we can't drive after the simulation ends
				}
				if runStop >= r.Stop.Time {
					continue // we can't arrive late
				}

				weight := r.Duration
				// it's ok to consider distanceFromStart after being updated!!
				startingTime := step + distanceFromStart
				if startingTime == r.Start.Time {
					weight += s.bonus
				}

				// update with this ride data because it has higher weight
				if weight > referenceWeight {
					referenceID = i
					referenceEnding = runStop
					referenceWeight = weight
				}
			}

			if referenceID < 0 {
				continue
			}

			r := rides[referenceID]

			// Update rides list to remove running or already done rides
			delete(rides, referenceID)
			// assign ride to vehicle

			v.Rides = append(v.Rides, r)
			v.OccupiedUntil = referenceEnding
			//r.Done = true
			v.Position = r.Stop.Position

			if len(rides) == 0 {
				break sim
			}
		}
	}

	return nil
}

func (s *Simulation) SprintInput() string {
	inStr := ""
	inStr = fmt.Sprintf("%v %v %v %v %v %v\n", s.rows, s.cols, s.fleetSize, s.ridesNumber, s.bonus, s.SimulationDuration)
	for _, r := range s.rides {
		inStr += fmt.Sprintf("%v %v %v %v %v %v\n", r.Start.X, r.Start.Y, r.Stop.X, r.Stop.Y, r.Start.Time, r.Stop.Time)
	}
	return inStr
}
func (s *Simulation) SprintOutput() string {
	outStr := ""
	for _, v := range s.vehicles {
		outStr += fmt.Sprintf("%v ", len(v.Rides))
		for _, r := range v.Rides {
			outStr += fmt.Sprintf("%v ", r.ID)
		}
		outStr += "\n"
	}
	return outStr
}

func (s *Simulation) DumpResult(str string) error {
	file, err := os.Create(s.outFileName)
	if err != nil {
		return errors.Wrap(err, "error opening output file")
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	defer writer.Flush()
	_, err = writer.WriteString(str)
	if err != nil {
		return errors.Wrap(err, "error writing output file")
	}
	return nil
}

type Vehicles []*Vehicle
type Vehicle struct {
	ID            int
	Position      Position
	Rides         Rides
	OccupiedUntil int
}

func NewVehicle(ID int) *Vehicle {
	return &Vehicle{ID: ID, Position: Position{0, 0}, Rides: Rides{}, OccupiedUntil: -1}
}

type Rides []Ride

//func (rs Rides) Len() int           { return len(rs) }
//func (rs Rides) Swap(i, j int)      { rs[i], rs[j] = rs[j], rs[i] }
//func (rs Rides) Less(i, j int) bool { return rs[i].Weight > rs[j].Weight } // reverse order, higher the weight, better the ride

type Ride struct {
	ID    int
	Start Instant
	Stop  Instant
	//Weight   int
	Duration int
	//Ending int
	//Done     bool
}

func NewRide(i, ax, ay, t1, bx, by, t2 int) Ride {
	r := Ride{
		ID:    i,
		Start: Instant{Position: Position{X: ax, Y: ay}, Time: t1},
		Stop:  Instant{Position: Position{X: bx, Y: by}, Time: t2},
		//Done:  false,
	}
	r.Duration = SpaceDistance(r.Start.Position, r.Stop.Position)
	return r
}

type Position struct {
	X, Y int
}

type Instant struct {
	Position
	Time int
}

func SpaceDistance(a, b Position) int {
	return int(math.Abs(float64(a.X-b.X)) + math.Abs(float64(a.Y-b.Y)))
}

func main() {
	folder := filepath.Join("problem", "data")
	inputFilesNames, err := filepath.Glob(filepath.Join(folder, "*.in"))
	if err != nil {
		logrus.Fatal(errors.Wrap(err, "can't glob input files"))
	}
	for _, fileName := range inputFilesNames {
		t0 := time.Now()
		fmt.Printf("Running file %v -> ", filepath.Base(fileName))
		s, err := NewSimulation(fileName, "")
		if err != nil {
			fmt.Println(errors.Wrap(err, "\ncan't init simulation"))
			os.Exit(1)
		}

		err = s.Run()
		if err != nil {
			fmt.Println(errors.Wrap(err, "\nerror running the simulation"))
			os.Exit(1)
		}

		// Dump result
		str := s.SprintOutput()
		s.DumpResult(str)

		t1 := time.Now()
		fmt.Printf("done in %v\n", t1.Sub(t0))

	}
}
